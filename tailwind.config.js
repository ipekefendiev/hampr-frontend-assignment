/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    extend: {
      colors: {
        primary: '#217AFF',
        gray: {
          lightest: '#EEEEEE',
          lighter: '#999999',
          light: '#777777',
          normal: '#666666',
          dark: '#555555',
        },
        background: '#F5FDFF',
        'row-background': '#EDF5FF',
        red: '#FF0000',
      },
      backgroundImage: {
        'custom-divider-y':
          'linear-gradient(180deg, rgba(0, 0, 0, 0) 0%, #000000 51.04%, rgba(0, 0, 0, 0) 100%)',
        'custom-divider-x':
          'linear-gradient(90deg, rgba(0, 0, 0, 0) 0%, #000000 51.04%, rgba(0, 0, 0, 0) 100%)',
      },
    },
  },
  plugins: [],
};
