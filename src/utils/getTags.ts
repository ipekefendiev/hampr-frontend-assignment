import { Character } from 'types';

export const getTags = (data: Character[]) => {
  const flattenedTags = data
    .map((character) => character.tags?.map((tag) => tag.tag_name))
    .flatMap((tag) => tag)
    .filter((tag) => tag);
  const tags = Array.from(new Set(flattenedTags));

  return tags;
};
