// https://kentcdodds.com/blog/how-to-use-react-context-effectively
// https://www.newline.co/@bespoyasov/how-to-use-usereducer-with-typescript--3918a332

import { createContext, useContext, useReducer, Dispatch } from 'react';
import {
  Action,
  ActionKind,
  ChampionContextState,
  Character,
  CharacterTag,
} from 'types';
import { getTags } from 'utils/getTags';

import jsonData from './data/characters.json';

const data = jsonData as Character[];

const initialState = {
  data,
  selectedChampions: [] as Character[],
  filteredChampions: data,
  tags: [...getTags(data), 'My Team'],
  // Filters could be stored in URL search params also.
  // It would be more conventional.
  filters: {
    search: '',
    tags: [],
  },
} as ChampionContextState;

const ChampionContext = createContext<{
  state: ChampionContextState;
  dispatch: Dispatch<Action>;
}>({ state: initialState, dispatch: () => {} });

const championsReducer = (
  state: ChampionContextState,
  action: Action
): ChampionContextState => {
  switch (action.type) {
    case ActionKind.TOGGLE_CHAMPION: {
      const isChampionSelected = state.selectedChampions.some(
        (champion) => champion.id === action.payload.id
      );

      const selectedChampions = isChampionSelected
        ? state.selectedChampions.filter(
            (champion) => champion.id !== action.payload.id
          )
        : [...state.selectedChampions, action.payload];

      return {
        ...state,
        selectedChampions,
      };
    }
    case ActionKind.SET_SEARCH_FILTER: {
      const searchValue = action.payload;

      return {
        ...state,
        filters: {
          ...state.filters,
          search: searchValue,
        },
      };
    }
    case ActionKind.TOGGLE_TAG_FILTER: {
      const filterTag = action.payload;
      const isFilterTagExists = state.filters.tags.includes(filterTag);

      const tags = isFilterTagExists
        ? state.filters.tags.filter((tag) => tag !== filterTag)
        : [...state.filters.tags, filterTag];

      return {
        ...state,
        filters: {
          ...state.filters,
          tags,
        },
      };
    }
    case ActionKind.FILTER_CHAMPIONS: {
      const searchValue = state.filters.search;
      const filterTags = state.filters.tags;
      const champions = state.filters.tags.includes('My Team')
        ? state.selectedChampions
        : state.data;

      const hasCharacterNameIncludesSearchValue = (
        championName: Character['name']
      ) => championName.toLowerCase().includes(searchValue.toLowerCase());

      const hasTagNameIncludesSearchValue = (tags: CharacterTag[]) =>
        tags?.some((tag: CharacterTag) =>
          tag.tag_name.toLowerCase().includes(searchValue.toLowerCase())
        );

      const filteredChampions = champions
        .filter(
          // Filter by search value
          (champion) =>
            hasCharacterNameIncludesSearchValue(champion.name) ||
            hasTagNameIncludesSearchValue(champion.tags)
        )
        .filter(
          // Filter by tags
          (champion) => {
            if (filterTags.length) {
              return filterTags
                .filter((filterTag) => filterTag !== 'My Team')
                .every((tag) =>
                  champion.tags?.some(
                    (championTag) => championTag.tag_name === tag
                  )
                );
            }
            return champion;
          }
        );

      return {
        ...state,
        filteredChampions,
      };
    }
    case ActionKind.CLEAR_TAG_FILTERS: {
      return {
        ...state,
        filters: {
          ...state.filters,
          tags: [],
        },
      };
    }

    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
};

type ProviderProps = {
  children: React.ReactNode;
};

const ChampionsProvider: React.FC<ProviderProps> = ({ children }) => {
  const [state, dispatch] = useReducer(championsReducer, initialState);

  const value = { state, dispatch };

  return (
    <ChampionContext.Provider value={value}>
      {children}
    </ChampionContext.Provider>
  );
};

const useChampions = () => {
  const context = useContext(ChampionContext);

  if (context === undefined) {
    throw new Error('useChampions must be used within a ChampionsProvider');
  }
  return context;
};

export { ChampionsProvider, useChampions };
