import { ChampionsProvider } from 'ChampionContext';
import SelectedChampions from 'components/SelectedChampions';
import Search from 'components/Search';
import Tags from 'components/Tags';
import ChampionTable from 'components/ChampionTable';
import Header from 'components/Header';

const App = () => (
  <ChampionsProvider>
    <Header />
    <div className="flex flex-col items-center px-4 pb-4 md:px-16 md:pb-16 pt-[140px]">
      <SelectedChampions />
      <Search />
      <Tags />
      <ChampionTable />
    </div>
  </ChampionsProvider>
);

export default App;
