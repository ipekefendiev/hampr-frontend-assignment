export enum ABILITY {
  POWER = 'Power',
  MOBILITY = 'Mobility',
  TECHNIQUE = 'Technique',
  SURVIVABILITY = 'Survivability',
  ENERGY = 'Energy',
}

export type AbilityName =
  | 'Mobility'
  | 'Technique'
  | 'Survivability'
  | 'Power'
  | 'Energy';

export interface CharacterAbility {
  abilityName: ABILITY;
  abilityScore: number;
}

export interface CharacterTag {
  slot: number;
  tag_name: string;
}

export interface Character {
  id: number;
  name: string;
  quote: string;
  image: string;
  thumbnail: string;
  universe: string;
  abilities: CharacterAbility[];
  tags: CharacterTag[];
}

export enum ActionKind {
  TOGGLE_CHAMPION,
  SET_SEARCH_FILTER,
  TOGGLE_TAG_FILTER,
  FILTER_CHAMPIONS,
  CLEAR_TAG_FILTERS,
}

export type Action = {
  type: ActionKind;
  payload?: any;
};

export interface ChampionContextState {
  data: Character[];
  selectedChampions: Character[];
  filteredChampions: Character[];
  tags: CharacterTag['tag_name'][];
  filters: {
    search: string;
    tags: CharacterTag['tag_name'][];
  };
}
