import { ButtonHTMLAttributes } from 'react';
import clsx from 'clsx';

interface Props extends ButtonHTMLAttributes<HTMLButtonElement> {
  children: string;
  className?: string;
}

const Tag: React.FC<Props> = (props: Props) => {
  const { children, className = '', ...rest } = props;

  return (
    <button
      type="button"
      className={clsx(
        'rounded-[20px] py-2.5 px-3 border border-primary text-primary text-lg hover:cursor-pointer',
        className
      )}
      {...rest}
    >
      {children}
    </button>
  );
};

export default Tag;
