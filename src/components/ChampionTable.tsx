import {
  flexRender,
  getCoreRowModel,
  useReactTable,
} from '@tanstack/react-table';
import clsx from 'clsx';

import { useChampions } from 'ChampionContext';
import useChampionTableColumns from 'hooks/useChampionTableColumns';
import { ActionKind } from 'types';

const ChampionTable = () => {
  const { state, dispatch } = useChampions();
  const columns = useChampionTableColumns();

  const table = useReactTable({
    data: state.filteredChampions,
    columns,
    getCoreRowModel: getCoreRowModel(),
  });

  return (
    <div className="h-[800px] w-full overflow-auto">
      <table className="w-full">
        <thead>
          {table.getHeaderGroups().map((headerGroup) => (
            <tr key={headerGroup.id}>
              {headerGroup.headers.map((header) => (
                <th
                  key={header.id}
                  className="px-5 py-2.5 text-xl font-bold text-left sticky top-0 bg-background"
                >
                  {header.isPlaceholder
                    ? null
                    : flexRender(
                        header.column.columnDef.header,
                        header.getContext()
                      )}
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody className="shadow-xl">
          {table.getRowModel().rows.map((row) => {
            const isChampionSelected = state.selectedChampions.some(
              (champion) => champion.id === row.original.id
            );

            return (
              <tr
                key={row.id}
                className={clsx(
                  'hover:bg-row-background rounded-[10px] cursor-pointer border-b border-gray-lightest',
                  isChampionSelected ? 'bg-row-background' : 'bg-white'
                )}
                onClick={() =>
                  dispatch({
                    type: ActionKind.TOGGLE_CHAMPION,
                    payload: row.original,
                  })
                }
              >
                {row.getVisibleCells().map((cell) => (
                  <td key={cell.id} className="p-5">
                    {flexRender(cell.column.columnDef.cell, cell.getContext())}
                  </td>
                ))}
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

export default ChampionTable;
