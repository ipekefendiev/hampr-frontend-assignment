import { useEffect, useState } from 'react';
import clsx from 'clsx';

import { useChampions } from 'ChampionContext';
import { ReactComponent as SearchIcon } from 'img/search-icon.svg';
import { ActionKind } from 'types';

const Search = () => {
  const [searchValue, setSearchValue] = useState('');
  const { dispatch } = useChampions();

  useEffect(() => {
    // workaround for re-render problem with filtering champions table
    dispatch({
      type: ActionKind.SET_SEARCH_FILTER,
      payload: searchValue,
    });
    dispatch({
      type: ActionKind.FILTER_CHAMPIONS,
    });
  }, [searchValue, dispatch]);

  return (
    <div
      className={clsx(
        'w:4/5 md:w-2/5 relative mt-9 mb-10',
        'before:absolute before:bg-custom-divider-x lg:before:w-[920px] md:before:w-[680px] sm:before:w-[620px] before:w-[280px] before:h-[1px] before:inset-1/2 before:-translate-x-1/2 before:-z-[1]'
      )}
    >
      <SearchIcon className="absolute inset-y-1/2 left-2.5 -translate-y-1/2" />
      <input
        className="border border-gray-light rounded p-2.5 pl-10 placeholder:text-gray-dark w-full bg-white"
        placeholder="Search Characters..."
        value={searchValue}
        onChange={(e) => setSearchValue(e.target.value)}
      />
    </div>
  );
};

export default Search;
