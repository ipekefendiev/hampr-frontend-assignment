import { FC } from 'react';
import { useChampions } from 'ChampionContext';
import clsx from 'clsx';

import useAverageStats from 'hooks/useAverageStats';
import { ABILITY, ActionKind } from 'types';
import Avatar from './Avatar';

const ChampionStats: FC = () => {
  const calculatedAverages = useAverageStats();

  return (
    <div>
      <ul className="grid grid-cols-2 gap-4 sm:gap-0 sm:grid-cols-5">
        {Object.values(ABILITY).map((ability, index) => (
          <li
            key={index}
            className={clsx(
              'flex flex-col gap-5 items-center justify-center px-2 [&:nth-child(3)]:relative [&:nth-child(3)]:col-span-2 sm:[&:nth-child(3)]:col-span-1',
              '[&:nth-child(3)]:before:absolute [&:nth-child(3)]:before:h-[100px] [&:nth-child(3)]:before:w-[1px] [&:nth-child(3)]:before:bg-custom-divider-y [&:nth-child(3)]:before:left-0',
              '[&:nth-child(3)]:after:absolute [&:nth-child(3)]:after:h-[100px] [&:nth-child(3)]:after:w-[1px] [&:nth-child(3)]:after:bg-custom-divider-y [&:nth-child(3)]:after:right-0'
            )}
          >
            <p>{ability}</p>
            <p className="font-bold text-2xl">{calculatedAverages[ability]}</p>
          </li>
        ))}
      </ul>
      <p className="mt-2.5 text-gray-normal text-xs">
        * Totals as average for squad
      </p>
    </div>
  );
};

const SelectedChampions = () => {
  const { state, dispatch } = useChampions();

  return (
    <div className="flex flex-col items-center">
      <h1 className="font-bold text-2xl mb-4">
        {state.selectedChampions.length
          ? 'Your champions!'
          : 'Select your squad to defend earthrealm'}
      </h1>
      <div className="flex flex-wrap gap-2 mb-4">
        {state.selectedChampions.map((champion) => (
          <div
            key={champion.id}
            className={clsx(
              'relative rounded-full cursor-pointer',
              'after:absolute after:rounded-full hover:after:content-["Remove"] after:z-10 after:text-white after:w-full after:h-full after:inset-1/2 after:-translate-x-1/2 after:-translate-y-1/2 after:flex after:items-center after:justify-center',
              'hover:after:bg-primary hover:after:bg-opacity-60 after:text-opacity-100'
            )}
            onClick={() =>
              dispatch({ type: ActionKind.TOGGLE_CHAMPION, payload: champion })
            }
          >
            <Avatar src={champion.image} />
          </div>
        ))}
      </div>
      <ChampionStats />
    </div>
  );
};

export default SelectedChampions;
