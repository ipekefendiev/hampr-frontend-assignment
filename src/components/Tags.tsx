import clsx from 'clsx';

import { useChampions } from 'ChampionContext';
import { ActionKind } from 'types';
import Tag from './Tag';

const Tags = () => {
  const { state, dispatch } = useChampions();

  return (
    <div className="flex flex-wrap gap-2 mb-14">
      {state.tags.map((tag, index) => {
        const isSelected = state.filters.tags.includes(tag);
        const capitalizedTag = tag.charAt(0).toUpperCase() + tag.slice(1);

        return (
          <Tag
            key={index}
            className={clsx(
              'hover:bg-primary hover:text-white hover:bg-opacity-50',
              isSelected && 'bg-primary text-white'
            )}
            onClick={() => {
              // workaround for re-render problem with filtering champions table
              dispatch({
                type: ActionKind.TOGGLE_TAG_FILTER,
                payload: tag,
              });
              dispatch({
                type: ActionKind.FILTER_CHAMPIONS,
              });
            }}
          >
            {capitalizedTag}
          </Tag>
        );
      })}
      <button
        type="button"
        className="text-gray-lighter text-xl hover:underline ml-4"
        onClick={() => {
          dispatch({ type: ActionKind.CLEAR_TAG_FILTERS });
          dispatch({
            type: ActionKind.FILTER_CHAMPIONS,
          });
        }}
      >
        Clear all
      </button>
    </div>
  );
};

export default Tags;
