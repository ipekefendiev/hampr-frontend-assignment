import Logo from 'img/Mortal-Kombat-Logo.png';

const Header = () => (
  <div className="h-[76px] bg-black flex justify-center fixed w-screen z-50">
    <img src={Logo} alt="mortal-kombat-logo" className="h-[76px] mt-[38px]" />
  </div>
);

export default Header;
