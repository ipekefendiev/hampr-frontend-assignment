type AvatarProps = {
  src: string;
  size?: string | number;
};

const Avatar: React.FC<AvatarProps> = ({ src, size = 80 }) => (
  <img
    src={src}
    alt="avatar"
    height={size}
    width={size}
    className="rounded-full border-[1px] border-solid border-primary"
  />
);

export default Avatar;
