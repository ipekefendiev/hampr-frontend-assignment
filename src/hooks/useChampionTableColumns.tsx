import { createColumnHelper } from '@tanstack/react-table';
import clsx from 'clsx';

import { useChampions } from 'ChampionContext';
import Avatar from 'components/Avatar';
import Tag from 'components/Tag';
import { ABILITY, Character } from 'types';

const columnHelper = createColumnHelper<Character>();

const useChampionTableColumns = () => {
  const { state } = useChampions();

  return [
    columnHelper.accessor((row) => row, {
      id: 'championName',
      header: () => 'Character',
      cell: (info) => {
        const champion = info.getValue();

        const isChampionSelected = state.selectedChampions.some(
          (champ) => champ.id === champion.id
        );

        return (
          <div className="flex gap-6 items-center">
            <input
              type="checkbox"
              className="w-6 h-6 accent-primary rounded-[4px] border hover:border-primary cursor-pointer"
              checked={isChampionSelected}
              readOnly
            />
            <Avatar src={champion.image} size={40} />
            <p>{champion.name}</p>
          </div>
        );
      },
    }),
    columnHelper.accessor('tags', {
      header: () => 'Tags',
      cell: (info) => {
        return (
          <div className="flex flex-wrap gap-4 min-w-[360px]">
            {info.getValue()?.map((tag, index) => {
              const capitalizedTag =
                tag.tag_name.charAt(0).toUpperCase() + tag.tag_name.slice(1);

              return (
                <Tag key={index} className="bg-white">
                  {capitalizedTag}
                </Tag>
              );
            })}
          </div>
        );
      },
    }),
    columnHelper.accessor((row) => row.abilities, {
      id: 'power',
      header: () => 'Power',
      cell: (info) =>
        info
          .getValue()
          ?.filter((ability) => ability.abilityName === ABILITY.POWER)
          .map((ability, index) => (
            <p
              key={index}
              className={clsx(
                'text-2xl font-bold',
                ability.abilityScore === 10 && 'text-red'
              )}
            >
              {ability.abilityScore}
            </p>
          )),
    }),
    columnHelper.accessor((row) => row.abilities, {
      id: 'mobility',
      header: () => 'Mobility',
      cell: (info) =>
        info
          .getValue()
          ?.filter((ability) => ability.abilityName === ABILITY.MOBILITY)
          .map((ability, index) => (
            <p
              key={index}
              className={clsx(
                'text-2xl font-bold',
                ability.abilityScore === 10 && 'text-red'
              )}
            >
              {ability.abilityScore}
            </p>
          )),
    }),
    columnHelper.accessor((row) => row.abilities, {
      id: 'technique',
      header: () => 'Technique',
      cell: (info) =>
        info
          .getValue()
          ?.filter((ability) => ability.abilityName === ABILITY.TECHNIQUE)
          .map((ability, index) => (
            <p
              key={index}
              className={clsx(
                'text-2xl font-bold',
                ability.abilityScore === 10 && 'text-red'
              )}
            >
              {ability.abilityScore}
            </p>
          )),
    }),
    columnHelper.accessor((row) => row.abilities, {
      id: 'survivability',
      header: () => 'Survivability',
      cell: (info) =>
        info
          .getValue()
          ?.filter((ability) => ability.abilityName === ABILITY.SURVIVABILITY)
          .map((ability, index) => (
            <p
              key={index}
              className={clsx(
                'text-2xl font-bold',
                ability.abilityScore === 10 && 'text-red'
              )}
            >
              {ability.abilityScore}
            </p>
          )),
    }),
    columnHelper.accessor((row) => row.abilities, {
      id: 'energy',
      header: () => 'Energy',
      cell: (info) =>
        info
          .getValue()
          ?.filter((ability) => ability.abilityName === ABILITY.ENERGY)
          .map((ability, index) => (
            <p
              key={index}
              className={clsx(
                'text-2xl font-bold',
                ability.abilityScore === 10 && 'text-red'
              )}
            >
              {ability.abilityScore}
            </p>
          )),
    }),
  ];
};

export default useChampionTableColumns;
