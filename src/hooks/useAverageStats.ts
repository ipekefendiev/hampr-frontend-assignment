import { useChampions } from 'ChampionContext';
import { ABILITY } from 'types';

const useAverageStats = () => {
  const { state } = useChampions();

  const getAverageByAbilityName = (abilityName: ABILITY) => {
    const value =
      state.selectedChampions.reduce((acc, current) => {
        const ability = current.abilities.find(
          (ability) => ability.abilityName === abilityName
        )!;

        return ability.abilityScore + acc;
      }, 0) / state.selectedChampions.length;

    const isValueFloatType = (val: number) => {
      return Number(val) === val && val % 1 !== 0;
    };

    return isValueFloatType(value) ? value.toFixed(2) : value || '-';
  };

  const calculatedAverages = {
    [ABILITY.POWER]: getAverageByAbilityName(ABILITY.POWER),
    [ABILITY.MOBILITY]: getAverageByAbilityName(ABILITY.MOBILITY),
    [ABILITY.TECHNIQUE]: getAverageByAbilityName(ABILITY.TECHNIQUE),
    [ABILITY.SURVIVABILITY]: getAverageByAbilityName(ABILITY.SURVIVABILITY),
    [ABILITY.ENERGY]: getAverageByAbilityName(ABILITY.ENERGY),
  };

  return calculatedAverages;
};

export default useAverageStats;
